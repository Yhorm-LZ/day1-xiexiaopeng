**O(objective)**:了解了bootcamp的课程规划，学会了PDCA流程进行工作与学习，学习了概念图的画法，学习了使用ORID进行每日总结，学习了standup meeting的概念。



**R(Reflective)**:impressive



**I(Interpretive)**:PDCA流程可以用于今后的学习工作，便于快速调整工作与学习计划。ORID可以及时对今日学习的内容进行总结，便于回顾知识与发现不足。



**D(Decisional)**:今后学习与工作会多尝试使用PDCA对流程进行规划，使用ORID的方式进行每日小节。